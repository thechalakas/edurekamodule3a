//
//  ViewController.swift
//  EdurekaModule3a
//
//  Created by Jay on 08/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate
{

    //lets get some outlets.
    //outlets can be used to connect stuff
    
    //outlet for the hello world label
    @IBOutlet var helloLabel: UILabel!
    
    //we need an outlet for input text field.
    @IBOutlet var inputTextField: UITextField!
    
    //now that we have a delegate linked lets trigger this when something is typed
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        print("textFieldShouldBeginEditing - You have typed" + textField.text!)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        print("textFieldDidBeginEditing - You have typed" + textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        print("textFieldShouldReturn - You have pressed enter" + textField.text!)
        return true
    }
    
    //lets write a function for that button
    @IBAction func clickMeButton(_ sender: UIButton)
    {

        print("You have entered clickMeButton")
        
        //lets change the text of the hello world here.
        helloLabel.text = "You clicked the button. Thank you so much!"
        
        print("You have left clickMeButton")
    }
    
    //lets write another function to reset the text back to beginning
    @IBAction func resetButton(_ sender: UIButton)
    {
        
        print("You have entered resetButton")
        
        //lets change the text of the hello world here.
        helloLabel.text = "Hello World"
        
        print("You have left resetButton")
    }
    
    //lets write a function that will dismiss the keyboard
    @IBAction func closeKeyBoard(_ sender: UITapGestureRecognizer)
    {
        
        print("You have entered closeKeyBoard")
        
        //lets hide the keyboard
        inputTextField.resignFirstResponder()
        
        print("You have left closeKeyBoard")
    }
    
    
    //we need another segue similar to segueToView2
    //we will send some data to the other controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        print("You have entered prepare")
        let someVariable = "The Dark Knight"
        //var viewControllerReceiver = segue.destinationViewController as? ViewControllerReceiver
        let secondController = segue.destination as? ViewController2
        secondController?.receivedMovieName = someVariable
        print("You have left prepare")
    }

    //lets write a function to segue programatically.
    @IBAction func segueToView2(_ sender: UIButton)
    {
        print("You have entered segueToView2")
        
        //lets write the segue code here.
        performSegue(withIdentifier: "View1ToView2", sender: self)
        
        print("You have left segueToView2")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("viewDidLoad has loaded")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

