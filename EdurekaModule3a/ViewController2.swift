//
//  ViewController2.swift
//  EdurekaModule3a
//
//  Created by Jay on 11/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class ViewController2: UIViewController
{
    //we will put the received value here.
    var receivedMovieName=""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("ViewController2 - viewDidLoad has loaded")
        print("The received value is "+receivedMovieName)
    }
}
