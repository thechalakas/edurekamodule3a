# navigation and keyboard

Stuff that is available in this repo.

1. navigation between two pages.
2. input boxes.
3. collect input. 
4. store and send data between two pages. 
5. make keyboard come and dissapear.
6. using buttons.
7. buttons reacting and manipulating text. 

**references and links**

no specific references that I can talk about. 

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 